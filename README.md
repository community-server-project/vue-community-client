# Vue Community Video

This is the frontend for the Vue Community Video. The server can be found here [Community Video Server](https://gitlab.com/community-server-project/vue-community-video-server).

### Credits

- Play button icon made by [Creaticca Creative Agency](https://www.flaticon.com/authors/creaticca-creative-agency) from [www.flaticon.com](https://www.flaticon.com/).
- Sound effects obtained from [https://www.zapsplat.com](https://www.zapsplat.com)

### Vue Default commands
```bash
yarn install # Project setup
yarn serve   # Compiles and hot-reloads for development
yarn build   # Compiles and minifies for production
yarn lint    # Lints and fixes files
```

### Vue Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
