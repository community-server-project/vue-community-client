import Vue from 'vue';
import Vuex from 'vuex';

import {VD_PLAYING} from '@/store/constants.js';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    roomName: '',
    roomPass: '',
    capacity: 0,
    admin: '',
    userNames: [],
    restricted: true,
    videoID: '',
    videoState: VD_PLAYING,
    videoTime: 0,
    inRoom: false,
    myUserName: '',
    notifications: [],
  },
  mutations: {
    updateRoom(state, payload) {
      state.roomName = payload.roomName;
      state.roomPass = payload.roomPass;
      state.capacity = payload.capacity;
      state.admin = payload.admin;
      state.userNames = payload.userNames;
      state.restricted = payload.restricted;
      state.videoID = payload.videoID;
      state.videoState = payload.videoState;
      state.videoTime = payload.videoTime;
      state.inRoom = true;
    },
    leaveRoom(state) {
      state.roomName = '';
      state.roomPass = '';
      state.capacity = 0;
      state.admin = '';
      state.userNames = [];
      state.restricted = true;
      state.videoID = '';
      state.videoState = VD_PLAYING;
      state.videoTime = 0;
      state.inRoom = false;
    },
    updateVideoID(state, payload) { state.videoID = payload.videoID; },
    updateVideoState(state, payload) { state.videoState = payload.videoState; },
    updateVideoTime(state, payload) { state.videoTime = payload.seekTime; },
    updateMyUserName(state, payload) { state.myUserName = payload.myUserName; },
    updateNotifications(state, payload) { state.notifications.push(payload.message); },
  },
});