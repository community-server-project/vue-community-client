export const JOIN_ROOM = 'join room';
export const JOIN_ROOM_RESPONSE = 'room created';
export const JOIN_ROOM_LISTENER = 'someone joined';
export const LEAVE_ROOM = 'leave room';
export const LEAVE_ROOM_RESPONSE = 'leaving room';
export const LEAVE_ROOM_LISTENER = 'someone left';
export const CHANGE_VIDEO = 'change video';
export const CHANGE_VIDEO_LISTENER = 'listen for change video';
export const SEEK = 'seek in video';
export const SEEK_LISTENER = 'listen to seeking';
export const CHANGE_STATE = 'change video state';
export const CHANGE_STATE_LISTENER = 'change video state listener';
export const SEND_MESSAGE = 'send message';
export const SEND_MESSAGE_LISTENER = 'sender message listener';

export const VD_PLAYING = 'playing';
export const VD_PAUSED = 'paused';