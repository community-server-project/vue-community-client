import Vue from 'vue';
import socketIOClient from 'socket.io-client';
import {
  JOIN_ROOM, JOIN_ROOM_RESPONSE, JOIN_ROOM_LISTENER,
  CHANGE_VIDEO, CHANGE_VIDEO_LISTENER, LEAVE_ROOM, LEAVE_ROOM_LISTENER,
  SEEK, SEEK_LISTENER, CHANGE_STATE, CHANGE_STATE_LISTENER,
  SEND_MESSAGE, SEND_MESSAGE_LISTENER,
} from './store/constants.js';
import store from './main.js';

import sound from '@/assets/notification.mp3';
import welcome from '@/assets/welcome.wav';
import leaveSound from '@/assets/leaveSound.wav';

const socket = socketIOClient(process.env.VUE_APP_SOCKET_URL);

socket.on(SEND_MESSAGE_LISTENER, function(data) {
  const audio = new Audio(sound);
  audio.play();
  const message = `${data.userName}: ${data.message}`;
  store.commit('updateNotifications', {message});
});

socket.on(JOIN_ROOM_LISTENER, function(data) {
  const audio = new Audio(welcome);
  audio.play();
  store.commit('updateNotifications', data);
});

socket.on(LEAVE_ROOM_LISTENER, function(data) {
  const audio = new Audio(leaveSound);
  audio.play();
  store.commit('updateNotifications', data);
});

socket.on(JOIN_ROOM_RESPONSE, function(data) {
  if (data.error) {
    Vue.notify({group: 'foo', title: 'Error!', text: data.error, position: 'top center', type: 'error'});
  } else {
    store.commit('updateRoom', data);
  }
});

socket.on(CHANGE_VIDEO_LISTENER, function(data) {
  if (data.error) {
    Vue.notify({group: 'foo', title: 'Error!', text: data.error, type: 'error'});
  } else {
    store.commit('updateVideoID', data);
  }
});

socket.on(CHANGE_STATE_LISTENER, function(data) {
  if (data.error) {
    Vue.notify({group: 'foo', title: 'Error!', text: data.error, type: 'error'});
  } else {
    store.commit('updateVideoState', data);
  }
});

socket.on(SEEK_LISTENER, function(data) {
  if (data.error) {
    Vue.notify({group: 'foo', title: 'Error!', text: data.error, type: 'error'});
  } else {
    store.commit('updateVideoTime', data);
  }
});

socket.on('welcome', function(data) {
  console.log(data.message);
});

export default {
  login: (roomName, roomPass, userName) => {
    socket.emit(JOIN_ROOM, {roomName, roomPass, userName});
  },
  updateState: (videoState) => {
    socket.emit(CHANGE_STATE, {videoState});
  },
  updateURL: (videoURL) => {
    socket.emit(CHANGE_VIDEO, {videoURL});
  },
  updateTime: (seekTime) => {
    socket.emit(SEEK, {seekTime});
  },
  sendMessage: (message) => {
    socket.emit(SEND_MESSAGE, {message});
  },
  leaveRoom: () => {
    socket.emit(LEAVE_ROOM);
    store.commit('leaveRoom');
  },
  socket
};
