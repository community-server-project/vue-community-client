import Vue from 'vue'
import Vuikit from 'vuikit';
import VuikitIcons from '@vuikit/icons';
import VueYoutube from 'vue-youtube';
import Notifications from 'vue-notification';
import {store} from '@/store/store.js';

import App from './App.vue';

import '@vuikit/theme';

Vue.use(Vuikit);
Vue.use(VuikitIcons);
Vue.use(VueYoutube);
Vue.use(Notifications);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  store
}).$mount('#app')

export default store;
